import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { CarsService } from '../cars.service';
import { Car } from '../models';

@Component({
  selector: 'app-cars-list',
  templateUrl: './cars-list.component.html',
  styleUrls: ['./cars-list.component.scss']
})
export class CarsListComponent implements OnInit {
  cars: Car[] = [];
  @Output() editCarInfo = new EventEmitter<Car>();
  constructor(private carsService: CarsService) { }

  ngOnInit() {
    this.carsService.carsList.subscribe(cars => {
      this.cars = cars;
    });
  }
  showCarInfo(car: Car): void {
    this.editCarInfo.emit(car);
    this.carsService.editFormState.next(true);
  }
}

export class Car {
  public id: number;
  public model: string;
  public name: string;
  public year: string;
  public type: string;

  constructor() {
  }
}

import { Component, Input, OnInit } from '@angular/core';
import { Car } from '../models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CarsService } from '../cars.service';

@Component({
  selector: 'app-car-info-form',
  templateUrl: './car-info-form.component.html',
  styleUrls: ['./car-info-form.component.scss']
})
export class CarInfoFormComponent implements OnInit {
  carInfo = new Car();
  carFormGroup: FormGroup;
  editCar = false;
  constructor(private formBuilder: FormBuilder,
              private carsService: CarsService) { }

  ngOnInit() {
    this.carsService.editFormState.subscribe(state => {
      this.editCar = state;
    });
    this.buildCarInfoForm();
  }
  buildCarInfoForm(): void {
    this.carFormGroup = this.formBuilder.group({
      name: ['', [Validators.required]],
      model: ['', [Validators.required]],
      type: ['', [Validators.required]],
      year: ['', [Validators.required]]
    }, );
  }
  isDefaultInfo(car: Car): void {
    this.carInfo = car;
    this.carFormGroup.controls.name.setValue(car.name);
    this.carFormGroup.controls.model.setValue(car.model);
    this.carFormGroup.controls.type.setValue(car.type);
    this.carFormGroup.controls.year.setValue(car.year);
  }
  editCarInfo(): void {
    const editedCar: Car = {...this.carFormGroup.value, id: this.carInfo.id};
    this.carsService.changeCar(editedCar);
  }
  addCar(): void {
    this.carsService.addNewCar(this.carFormGroup.value);
    this.carFormGroup.reset();
  }
}

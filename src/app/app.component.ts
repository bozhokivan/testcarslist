import {Component, OnInit, ViewChild} from '@angular/core';
import { Car } from './models';
import {CarsService} from './cars.service';
import {CarInfoFormComponent} from './car-info-form/car-info-form.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  @ViewChild(CarInfoFormComponent, {static: false}) carInfoForm: CarInfoFormComponent;
  title = 'carsListFront';
  showForm = true;
  constructor(private carsService: CarsService) {}
  ngOnInit(): void {
  }

  addNewCar(): void {
    this.showForm = true;
    this.carsService.editFormState.next(false);
  }
  editCar(car: Car): void {
    this.carInfoForm.isDefaultInfo(car);
  }
}

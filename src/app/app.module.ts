import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CarsListComponent } from './cars-list/cars-list.component';
import { CarInfoFormComponent } from './car-info-form/car-info-form.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CarsService } from './cars.service';

@NgModule({
  declarations: [
    AppComponent,
    CarsListComponent,
    CarInfoFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
  ],
  providers: [
    CarsService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

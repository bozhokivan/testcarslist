import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Car } from './models';

@Injectable({
  providedIn: 'root'
})
export class CarsService {
  carsList = new BehaviorSubject<Car[]>([]);
  editFormState = new BehaviorSubject<boolean>(false);
  constructor() { }
  addNewCar(newCar: Car): void {
    const car = {...newCar, id: Math.floor(Math.random() * 1000000)};
    this.carsList.getValue().push(car);
  }
  changeCar(changedCar: Car): void {
    let allCars = this.carsList.value;
    allCars.map(car => {
      if (car.id === changedCar.id) {
        car.name = changedCar.name;
        car.model = changedCar.model;
        car.type = changedCar.type;
        car.year = changedCar.year;
      }
    });
    this.carsList.next(allCars);
  }
}
